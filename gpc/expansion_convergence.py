from proteus.iproteus import *
import numpy as np
Profiling.logLevel = 5
Profiling.verbose = True

mesh_size = 1001
Ns = np.arange(4, 11)

import burgers_gpc_1d_p as physics
import burgers_gpc_1d_dgp2_lim_n as numerics
pList = [physics]
nList = [numerics]
so = default_so
so.sList = [default_s]
opts.cacheArchive = True
so.archiveFlag = Archiver.ArchiveFlags.EVERY_USER_STEP

for N in Ns:
    so.name = pList[0].name = "gpc_dgp2_N{0:02d}".format(N)
    pList[0].coefficients = pList[0].gPCBurgersEqn(nu=1e-3, nc=N)
    pList[0].initialConditions = dict((k, pList[0].vkIC(k=k)) for k in range(N))
    pList[0].dirichletConditions = dict((i, pList[0].emptyBC) for i in range(N))
    pList[0].advectiveFluxBoundaryconditions = dict((i, pList[0].emptyBC) for i in range(N))
    pList[0].diffusiveFluxBoundaryConditions = dict((i, {}) for i in range(N))
    pList[0].periodicDirichletConditions = dict((i, pList[0].getPBC) for i in range(N))
    pList[0].fluxBoundaryConditions = dict((i, 'outFlow') for i in range(N))

    nList[0].timeIntegration = nList[0].SSPRKPIintegration
    # stepController = FixedStep
    nList[0].nSteps = 1000
    nList[0].stepController = nList[0].Min_dt_RKcontroller
    nList[0].nDTout = 1000
    nList[0].tnList = [float(n)/nList[0].nSteps*pList[0].T for n in range(nList[0].nSteps + 1)]

    nList[0].tnList.insert(1, 1e-6)
    nList[0].nn = mesh_size
    nList[0].femSpaces = dict(
        (i, nList[0].FemTools.DG_AffineQuadraticOnSimplexWithNodalBasis)
        #(i, FemTools.DG_AffineP3_OnSimplexWithMonomialBasis)
        for i in range(N))
    nList[0].periodicDirichletConditions = dict((i, nList[0].getPBC) for i in range(N))
    ns = NumericalSolution.NS_base(so, pList, nList, so.sList, opts)
    ns.calculateSolution(so.name)
    del ns
