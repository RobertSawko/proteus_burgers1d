\documentclass[compress,hyperref={pdfpagelabels=false}]{beamer}

\include{preamble}
\usepackage{pgf}
\usepackage{mathtools}
% \pdfpageattr {/Group << /S /Transparency /I true /CS /DeviceRGB >>}

%\makeatletter%
%\special{pdf: put @thispage <</Group << /S /Transparency /I true /CS /DeviceRGB>> >>}%
%\makeatother%

\defaultfontfeatures{Scale=MatchLowercase,Mapping=tex-text}

% \setmainfont{Liberation Serif}

\setmainfont{TeX Gyre Schola}
% \setmathfont{TeX Gyre Pagella Math}
% \setmainfont{Tex Gyre Pagella}
% \setmathfont{Tex Gyre Pagella Math}
%\AtBeginDocument{
  %\DeclareSymbolFont{pureletters}{T1}{lmss}{\mddefault}{it}
  %}

% \renewcommand{\textsc}{\textit}
% \usefonttheme{professionalfonts}

% \usepackage{minted}
%\usepackage{inconsolata}
\usepackage[style=authortitle,backend=biber]{biblatex}
\addbibresource{references.bib}

%\let\oldfootnotesize\footnotesize
%\renewcommand*{\footnotesize}{\oldfootnotesize\tiny}
\renewcommand{\footnotesize}{\tiny}

%Define the title and the author. Note [] contain the short names that will
%appear in footlines and headers.
\title{Uncertainty quantification in the solutions of partial differential
equation}
\author[R.Sawko \& E.Meese]{Robert Sawko and Ernst Meese}
%\email{r.sawko@cranfield.ac.uk}
\institute[]{Oil \& Gas Engineering Centre, Cranfield University}

%You can fix the day if you know the day of your presentation.
\date{26th of Februrary, Cranfield}

%The document
\begin{document}
  %This is to suppress the footline on the title slide
\setbeamertemplate{footline}{}
  %The title slide
\begin{frame}
  \maketitle
  \begin{center}
    \includegraphics[width=3cm]{figures/logo.pdf}%
  \end{center}
\end{frame}
%This is so that the title slide doesn't count as the first slide.
\addtocounter{framenumber}{-1}
\section[Intro]{Introduction}
\begin{frame}{In the last episode\ldots}
  \begin{itemize}
    \item Model problem was an ODE:
      \begin{gather}
        \frac{\textrm{d}f}{\textrm{d}t} = -c f, \quad f(0) = f_0
      \end{gather}
    \item $c$ was a random variable with either normal or uniform distribution.
    \item The difference between mean of the solution and the solution from the
      mean.
    \item Generalized polynomial chaos methods:
      \begin{itemize}
        \item stochastic Galerkin,
        \item collocation methods.
      \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}{Outline}
  \tableofcontents[hideallsubsections]
\end{frame}
\begin{frame}{Outline}
  \tableofcontents[currentsection,hideothersubsections]
\end{frame}
\begin{frame}{Model problem: Burgers' 1D equation}
  We are considering here 1D viscous Burgers' equation with forcing on a
  periodic domain:
  \begin{align}
    \pd{u}{t} + u\pd{u}{x} & = \nu \frac{\partial^2u}{\partial x^2} + \sigma f(x,t; \omega), \quad x \in [0, 2\pi], \quad t\geq0
    \\
    u(x,0; \omega) & =u_0(x; \omega)
    \\
    u(2\pi,t; \omega) & = u(0,t; \omega)
  \end{align}
  where $f$ is the forcing term, $\omega$ is an element from a sample space,
  $\sigma$ is the scaling of the forcing, $\nu$ is the viscosity. We are
  interested in the vanishing viscosity limit. This problem was considered by
  Heryrim (2014)\footfullcite{Heyrim2014}.
\end{frame}
\begin{frame}{General forms of IC and forcing}

  $u_0$, $f$, are represented in terms of series expansions involving sets of
  random variables $\mathbf{\xi}(\omega) = \{\xi_1(\omega), \cdots,
  \xi_m(\omega)\}$ $\mathbf{\eta}(\omega) = \{\eta_1(\omega), \cdots,
  \eta_\ell(\omega)\}$
  \begin{gather}
    u_0(x, \mathbf{\eta}) = \sum_{k = 1}^{\ell} \eta_k (\omega) \phi_k(x)
    \\
    f(x, t, \mathbf{\xi}) = \sum_{k = 1}^{m} \xi_k (\omega) \psi_k(x)
  \end{gather}
  $\ell$ is the dimensionality of IC and $m$ is the dimensionality of forcing.
\end{frame}
\begin{frame}{Some applications for stochastic Burgers' equation}
  \begin{itemize}
    \item Wave processes in acoustics and hydrodynamics.
    \item Modelling of vortex lines in high-temperature superconductors.
    \item Dislocation in disordered solids.
    \item Kinetic roughening of interfaces in epitaxial growth.
    \item Formation of large-scale structures in the universe.
    \item Constructive quantum field theory \footfullcite{Hairer2011}.
  \end{itemize}
\end{frame}
\begin{frame}{Objectives}
  \begin{enumerate}
    \item Reproduce Figure 1 and 3 from Heyrim's work.
    \item Suggest uncertainty description for single or two phase flow.
    \item Study feasibility of numerical modelling of uncertainty for two phase
      application.
  \end{enumerate}
  \begin{figure}[b]
    \centering
    \includegraphics[width=0.49\textwidth]{figures/F1.large.jpg}
    \includegraphics[width=0.49\textwidth]{figures/F3.large.jpg}
    \caption{Figure 1 and Figure 3 (right) from Heyrim's work}
  \end{figure}
\end{frame}
\begin{frame}{Proteus}
  \begin{itemize}
    \item Finite element code made available on Github \footfullcite{proteus}.
    \item Under active development.
    \item Supports discontinuous Galerkin methods.
    \item Hybrid Python/C++ code with python used at higher level.
    \item Can use \texttt{matplotlib} or ParaView for visualisation.
    \item Tutorials in IPython Notebook format.
  \end{itemize}
\end{frame}
\section[PDF]{PDF approach}
\begin{frame}{Outline}
  \tableofcontents[currentsection,hideothersubsections]
\end{frame}
\begin{frame}{Introducing solution PDF}
  Note that for fixed parameters we get a particular solution

  \begin{gather}
    u(x, t; \omega) = U(x, t; \mathbf{\eta}(\omega), \mathbf{\xi}(\omega))
  \end{gather}
  And the PDF for a particular velocity can be formed using selection property
  of Dirac's delta:
  \begin{align}
    p(x, t; a, \mathbf b) =
      \int_{-\infty}^\infty \! \ldots  \int_{-\infty}^\infty \!
      & \delta(a - U(x, t, \mathbf{A}_0, \mathbf{B})) \nonumber
      \\
      & \delta(\mathbf{b} - \mathbf{B})q(\mathbf{A}_0, \mathbf{B})
      \, \mathrm{d} \mathbf{A}_0 \mathbf{B}
  \end{align}
  The one-point one-time PDF can be obtained via integration:
  \begin{gather}
    p_u(x, t; a) = \int_{-\infty}^\infty \! \ldots  \int_{-\infty}^\infty \!
    p(x, t; a, \mathbf{b}) \, \mathrm{d}\mathbf{b}
  \end{gather}
\end{frame}
\begin{frame}{Single-point PDF equation}
  \begin{align}
    \pd{p(t)}{t}
    + \int_{-\infty}^a \! \pd{p}{x} \, \mathrm{d}a'
    + a \pd{p}{x}
    & =
    -\sigma f(x, t; \textbf{b})\pd{p}{a}
    \nonumber
    \\ 
    & 
    -\nu \pd{}{a} \left[ \left\langle \frac{\partial^2 u}{\partial x^2} \bigg| u\right\rangle p_u(t) \right]
  \end{align}
  where $\textbf{b}$ are the coefficients of the forcing term. $\left\langle
  \frac{\partial^2 u}{\partial x^2} \bigg| u\right\rangle$ is a conditional
  average that could be obtained from the sampling of the original 1D problem.
  It is a function of $x$ and $t$ and it captures the expected value of the
  second order term under the assumption of known $u$ value at $x$, $t$
  position.
\end{frame}
\begin{frame}{Initialisation}
  We are considering a problem where 
  \begin{gather}
    u_0 (x, \omega) = \sin(x) + \eta(\omega)
    \\
    f (x, t, \omega) = \xi(\omega) \sin(t),
  \end{gather}
  where $\xi$ and $\eta$ are independent zero-mean Gaussian random variables
  standard deviations of $\pi/10$ and $1$ respectively.

  For this problem the initial PDF is given by
  \begin{gather}
    p(x, 0; a, b) = \frac{5}{\pi^2} \exp\left[- \frac{(a - \sin(x))^2}{2}\right] 
    \exp\left[-50 \frac{b^2}{\pi^2}\right]
  \end{gather}
\end{frame}
\begin{frame}{Evolution of the PDF}
  \begin{center}
    \animategraphics[
      width=0.95\textwidth,
      loop,
      autoplay
    ]{8}{./animations/pdf/a.}{0000}{0100}

    Smooth bimodal PDF develops.
  \end{center}
\end{frame}
  \begin{frame}{Mesh convergence for PDF}
    \fixpdfrgb
    \begin{center}
      \includegraphics[width=0.25\textwidth]{./figures/plot_over_line.png}
      \\
      \input{./figures/pdf/pdf_mesh_convergence_dgp1.pgf}
      \input{./figures/pdf/pdf_mesh_convergence_dgp2.pgf}
      \\
      \input{./figures/pdf/pdf_mesh_convergence-legend.pgf}
    \end{center}
  \end{frame}
  \begin{frame}{PDF comparison}
    \fixpdfrgb
    \begin{center}
      \input{./figures/pdf/histogram-kin.pgf}
      \input{./figures/pdf/histogram-mc.pgf}
    \end{center}
    PDF obtained for $t=1s$. Monte-Carlo simulation of 50000 realization was used
    in this comparison. In a single core run the run took approximately a week.
    The PDF approach converged in less than 15 minutes on a slower machine.
  \end{frame}
  \begin{frame}{Mean and standard deviation}
    \fixpdfrgb
    \begin{center}
      \input{./figures/pdf/compare_mean.pgf}
      \input{./figures/pdf/compare_std.pgf}
      \\
      \input{./figures/pdf/compare_legend.pgf}
    \end{center}

    Comparison of mean and standard deviation profiles in the domain. The "naive
    solution" is the solution obtained from the mean perturbation to initial
    conditions and forcing i.e. \textbf{assuming that the unknown values are equal
    to the mean}.
  \end{frame}
  \begin{frame}{Shock statistics issue}
    \fixpdfrgb
    \small
    Recall that for $\mathcal{N}(0,\sigma_1^2) +  \mathcal{N}(0, \sigma_2^2) =
    \mathcal{N}(0, \sigma_1^2 + \sigma_2^2)$ and therefore
    \begin{gather}
      u_0 (x, \omega) = \frac{1}{\sqrt{m}} \sum_{k=1}^m 
      \left(
      \eta_{2k}(\omega) \sin(kx) + \eta_{2k-1}(\omega)\cos(kx)
      \right)
    \end{gather}
    has the same PDF for any $m$ but the realizations may look very differently:
    \begin{center}
      \input{./figures/initialization.pgf}
      \input{./figures/initialization_legend.pgf}
    \end{center}
  \end{frame}
  \begin{frame}{Two-point PDF equation and reduced PDF approaches}
    \fixpdfrgb
    \begin{itemize}
      \item Two-point PDF is given by
        \begin{align}
          p_2(x_1, x_2, t; a_1, a_2) =&  \int_{-\infty}^\infty \! \ldots  \int_{-\infty}^\infty \!
          \prod_{i=1}^2 \delta(a_i - U(x_i, t, \mathbf{A}_0, \mathbf{B})) \nonumber
          \\
          & \delta(\mathbf{b} - \mathbf{B})q(\mathbf{A}_0, \mathbf{B})
          \, \mathrm{d} \mathbf{A}_0 \mathbf{B}
        \end{align}
      \item An evolution equation can be derived
        \begin{gather}
          \pd{p_2(t)}{t} = \left[\mathcal{H}_0 + \sigma \mathcal{H}_1\right] p_2(t)
          \\
          \mathcal{H}_0 = -\sum_{i = 1}^2 \left(
          \int_{-\infty}^{a_i} \mathrm{d}a'_i\pd{}{x_i} + a_i \pd{}{x_i}
          \right),
          \\
          \mathcal{H}_1(t) = -\sum_{i = 1}^2 f(x_i, t, \mathbf{b}) \pd{}{a_i}
        \end{gather}
    \end{itemize}
  \end{frame}
  \section[Galerkin]{Stochastic Galerkin gPC}
  \begin{frame}{Outline}
    \fixpdfrgb
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
  \begin{frame}{Stochastic Galerkin Method - revision}
    \fixpdfrgb
    Generalized polynomial chaos (gPC) stochastic Galerkin method can be
    formulated as follows\footfullcite{Xiu2010}:
    \begin{enumerate}
      \item Choose orthogonal polynomial basis and assume the solution $u$ can be
        approximated by
        \begin{gather}
          \hat{u}_N(x, t, Z) = \sum_{i = 0}^N \hat{v}_i(x,t) \Phi_i(Z),
        \end{gather}
      \item plug this approximation into your original equation and perform a
        projection onto the set of basis function
        \begin{gather}
          \mathbb{E}\left[\pd{\hat{u}_N}{t} \Phi_k\right]
          + \mathbb{E} \left[\hat{u}_N \pd{\hat{u}_N}{x} \Phi_k\right]
          =
          \nu \mathbb{E} \left[
            \frac{\partial^2 \hat{u}_N}{\partial x^2}
            \Phi_k
          \right],
        \end{gather}
        for  $k = 0$, \ldots, $N$.
      \item Use the orthogonality $\mathbb{E}\left[\Phi_i \Phi_j\right] =
        \gamma_{i} \delta_{ij}$.
    \end{enumerate}
  \end{frame}
  \begin{frame}{gPC applied to Burgers' equation}
    The final form for the set of equations for the coefficients in gPC expansion
    is:
    \begin{gather}
      \pd{\hat{v}_k}{t}
      + \frac{1}{\gamma_k} \sum_{i=0}^N \sum_{j=0}^N \hat{v}_i \pd{\hat{v}_j}{x} e_{ijk}
      =
      \nu \frac{\partial^2 \hat{v}_k}{\partial x^2},
      \quad k=0, \cdots, N,
    \end{gather}
    where $e_{ijk} = \mathbb{E}\left[\Phi_i \Phi_j \Phi_k\right]$ and $\gamma_k =
    \mathbb{E}\left[\Phi_k^2\right]$.
  \end{frame}
  \begin{frame}{Example system for $N=2$}
    \fixpdfrgb
    \small
    \begin{gather}
      \begin{dcases}
        \pd{\hat{v}_0}{t} 
        + \hat{v}_0 \pd{\hat{v}_0}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_2 \pd{\hat{v}_2}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_0}{\partial x^2},
        \\
        \pd{\hat{v}_1}{t} 
        + \hat{v}_1 \pd{\hat{v}_0}{x} 
        + \hat{v}_0 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_2 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_1 \pd{\hat{v}_2}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_1}{\partial x^2},
        \\
        \pd{\hat{v}_2}{t} 
        + \hat{v}_0 \pd{\hat{v}_2}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + \hat{v}_2 \pd{\hat{v}_0}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + 4\hat{v}_2 \pd{\hat{v}_2}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_2}{\partial x^2},
      \end{dcases}
    \end{gather}
  \end{frame}
  \begin{frame}{Example system for $N=4$}
    \fixpdfrgb
    \tiny
    \begin{gather}
      \begin{dcases}
        \pd{\hat{v}_0}{t} 
        + \hat{v}_0 \pd{\hat{v}_0}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_2 \pd{\hat{v}_2}{x} 
        + 6\hat{v}_3 \pd{\hat{v}_3}{x} 
        + 24\hat{v}_4 \pd{\hat{v}_4}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_0}{\partial x^2},
        \\
        \pd{\hat{v}_1}{t} 
        + \hat{v}_1 \pd{\hat{v}_0}{x} 
        + \hat{v}_0 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_2 \pd{\hat{v}_1}{x} 
        + 2\hat{v}_1 \pd{\hat{v}_2}{x} 
        + 24\hat{v}_3 \pd{\hat{v}_4}{x} 
        + 24\hat{v}_4 \pd{\hat{v}_3}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_1}{\partial x^2},
        \\
        \pd{\hat{v}_2}{t} 
        + \hat{v}_0 \pd{\hat{v}_2}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + 3\hat{v}_1 \pd{\hat{v}_3}{x} 
        + \hat{v}_2 \pd{\hat{v}_0}{x} 
        + \hat{v}_1 \pd{\hat{v}_1}{x} 
        + 4\hat{v}_2 \pd{\hat{v}_2}{x} 
        + 12\hat{v}_2 \pd{\hat{v}_4}{x} 
        \\
        \quad \quad \quad \quad
        + 18\hat{v}_3 \pd{\hat{v}_3}{x} 
        + 3\hat{v}_3 \pd{\hat{v}_1}{x} 
        + 96\hat{v}_4 \pd{\hat{v}_4}{x} 
        + 12\hat{v}_4 \pd{\hat{v}_2}{x} 
        =
        \nu \frac{\partial^2 \hat{v}_2}{\partial x^2},
        \\
        \vdots
      \end{dcases}
    \end{gather}
  \end{frame}
  \begin{frame}{$N=4$ in orthonormal basis}
    \fixpdfrgb
    and in conservative form
    \begin{gather}
      \begin{dcases}
        \pd{\hat{v}_0}{t}
        + \frac{1}{2}\left(\pd{\hat{v}_0^2}{x}
        + \pd{\hat{v}_1^2}{x}
        + \pd{\hat{v}_2^2}{x}
        + \pd{\hat{v}_3^2}{x}
        + \pd{\hat{v}_4^2}{x}\right)
        =
        \nu \frac{\partial^2 \hat{v}_0}{\partial x^2},
        \\
        \pd{\hat{v}_1}{t}
        + \pd{}{x} \left(\hat{v}_0\hat{v}_1\right)
        + \sqrt{2} \pd{}{x}\left(\hat{v}_1\hat{v}_2\right)
        + \sqrt{3} \pd{}{x}\left(\hat{v}_3\hat{v}_4\right)
        =
        \nu \frac{\partial^2 \hat{v}_1}{\partial x^2},
        \\
        \pd{\hat{v}_2}{t}
        + \hat{v}_0 \pd{\hat{v}_2}{x}
        + \frac{\sqrt{2}}{2} \pd{\hat{v}_1^2}{x}
        + \frac{\sqrt{3}}{2}\pd{}{x}\left(\hat{v}_1 \hat{v}_3\right)
        + \sqrt{2}\pd{\hat{v}_2^2}{x}
        \\
        \quad \quad \quad \quad
        + \sqrt{3}\pd{}{x}\left(\hat{v}_2 \hat{v}_4\right)
        + \frac{3\sqrt{3}}{2}\hat{v}_3 \pd{\hat{v}_3^2}{x}
        + 2\sqrt{2} \pd{\hat{v}_4^2}{x}
        =
        \nu \frac{\partial^2 \hat{v}_2}{\partial x^2},
        \\
        \vdots
      \end{dcases}
    \end{gather}
  \end{frame}
  \begin{frame}{Transient behaviour}
    \begin{center}
      \animategraphics[
        width=0.95\textwidth,
        loop,
        autoplay
    ]{8}{./animations/gpc/a.}{0000}{0127}

    Evolution of expansion coefficients.
  \end{center}
\end{frame}
\begin{frame}{Comparison}
  \fixpdfrgb
  \begin{center}
    \input{./figures/gpc/gpc_mean.pgf}
    \input{./figures/gpc/gpc_std.pgf}
    \\
    \input{./figures/gpc/gpc_legend.pgf}
    \scriptsize
    \begin{tabular}[t]{{lcccccc}}
      \toprule
      gPC order & 4 & 5 & 6 & 7 & 8 & 9\\
      \midrule
      Time [s]& 210 & 1030 & - & 5721 & - & 26412\\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}
\section[Collocation]{Pseudospectral Collocation gPC} \begin{frame}{Outline}
  \fixpdfrgb
  \tableofcontents[currentsection,hideothersubsections]
\end{frame}
\begin{frame}{Method - Pseudospectral gPC}
  \fixpdfrgb
  For the expansion $u_N$ approximating the solutin $u$ of the Burgers' problem,
  \begin{equation*}
    u_N(x,t,Y,Z) = \sum_{|\mathbf{k}|=0}^{N} \hat{v}_\mathbf{k}(x,t)\Phi_\mathbf{k}(Y,Z)
  \end{equation*}
  the coefficients can be expressed as\footfullcite{Xiu2010}
  \begin{equation*}
    \hat{v}_\mathbf{k}(x,t) = \frac{1}{\pi\,\gamma_\mathbf{k}}\iint u(x,t,Y,Z)\,\boldsymbol{\Phi}_\mathbf{k}(Y,Z)\,e^{-Y^2/2}\,e^{-Z^2/2}\,dY\,dZ
  \end{equation*}
  \begin{itemize}
    \item $Y$ and $Z$ are standard normal variables
    \item $y=\eta$ and $Z=\frac{10}{\pi}\xi$.
      \begin{itemize}
        \item[$\eta$] Random variable for initial condition
        \item[$\xi$] Random variable for forcing term
      \end{itemize}
  \end{itemize}
  \vspace*{1ex}
\end{frame}
\begin{frame}{Method contd.}
  \fixpdfrgb
  \begin{itemize}
    \item Index $\mathbf{k}=(k_1,k_2)$ is a multi-index with
      $|\mathbf{k}|=k_1+k_2$.
    \item The $N$th degree polynomial basis is
      \begin{equation*}
        \boldsymbol{\Phi}_\mathbf{k}(Y,Z)=\Phi_{k_1}(Y)\Phi_{k_2}(Z),\quad
        0\leq |\mathbf{k}| \leq N
      \end{equation*}
      where $\Phi_{k}(\cdot)$ are the Hermite polynomials normalized to fit the
      standard normal distribution.
    \item The integral is approximated by cubature formed by a tensor product
      on the Gauss-Hermite quadrature.
      \begin{equation*}
        \hat{v}_\mathbf{k}(x,t) \approx \frac{1}{\pi\,\gamma_\mathbf{k}}\sum_{i=1}^Q
        \sum_{j=1}^Q u(x,t,Y_i,Z_j)\boldsymbol{\Phi}_{\mathbf{k}}(Y_i,Z_j)\alpha_i\alpha_j
      \end{equation*}
      where $Q$ is the number of quadrature points and $\alpha$ the quadrature weights.
    \item Realizations of the solution has to be made on the grid defined by the
      cubature points $(Y_i,Z_j)$, i.e., $Q\times Q$ points.
  \end{itemize}
\end{frame}
\begin{frame}{Results}
  \fixpdfrgb
  \input{./figures/colloc/colloc_odd_solutions.pgf}\\
  \vspace*{-2ex}
  \input{./figures/colloc/colloc_even_solutions.pgf}\\
  \vspace*{-2ex}
  \begin{itemize}
    \item $Q=N+1$ for accuracy balance expansion v.s. cubature.
    \item Note difference between even and odd order expansions.
  \end{itemize}
\end{frame}
\begin{frame}{Results, contd.}
  \fixpdfrgb
  \input{./figures/colloc/colloc_avg_solutions.pgf}\\
  \begin{itemize}
    \item Calculating expansion of order $N-1$ with cubatures using $N\times N$ and
      $N+1\times N+1$ points.
    \item Averaging the expansion coefficients over the results from the two
      cubatures.
  \end{itemize}
\end{frame}
\begin{frame}{Convergence}
  \fixpdfrgb
  \input{./figures/colloc/colloc_error.pgf}
  \begin{itemize}
    \item A single cubature gives less than linear convergence.
    \item Average between two cubatures give linear convergence.
  \end{itemize}
\end{frame}
\begin{frame}{Probability Density Function}
  \input{./figures/colloc/colloc_pdf.pgf}\\\vspace*{-1ex}
  PDFs for cubatures with $n$ and $n+1$ points, and their average.
\end{frame}
\section[HD problem]{Into higher dimensionality}
\begin{frame}{Outline}
  \fixpdfrgb
  \tableofcontents[currentsection,hideothersubsections]
\end{frame}
\begin{frame}{High-dimension problems}
  \begin{columns}
    \begin{column}{0.59\textwidth}
      \small
      We assume $u_0$ can be described as a stochastic process with the
      following auto-correlation function
      \begin{equation}
        C(x,y) = \mathrm{exp}(-|x-y|/l_c)
      \end{equation}
      where $l_c$ is the correlation length. 
    \end{column}
    \begin{column}{0.39\textwidth}
      \includegraphics[width=\textwidth]{./figures/hd/covariance.pdf}
    \end{column}
  \end{columns}
  \small
  The representation of this stochastic process is done via KL expansion which
  can be obtained analytically for that process. The representation will take
  the following form.
  \begin{equation}
    u_0(x, \omega) = 
    a_0 (\eta_0 + \sin(x)) + \sum_{k=1}^m \sqrt{\lambda_k}\eta_k \Psi_k(x)
  \end{equation}
  where $\eta_k$ are are uncorrelated random variables with standard normal
  distribution, $\lambda_k$ and $\Psi_k$ are the eigenvalues and eigenfunctions
  of the correlation function $C$.
\end{frame}
\begin{frame}{Initialisation cont.}
  \fixpdfrgb
  \small
  \begin{itemize}
    \item The eigen-functions are functions that satisfy:
      \begin{gather}
        \int \! \Psi_k(y) C(x,y)\, \mathrm{d}y = \lambda_k \Psi_k(x)
      \end{gather}
    \item For the exponential covariance eigenfunctions take the following
      form\footfullcite{Ghanem1991}:
      \small
      \begin{align}
        \lambda_{2n} = \frac{2c} {\omega_{2n}^2 + c^2} && 
        \Psi_{2n}(x) = \frac
        {\cos(\omega_{2n} x)}
        {\sqrt{\pi + \frac{\sin(2\omega_{2n}a)}{2\omega_{2n}}}}
        \\
        \lambda_{2n+1} = \frac{2c}{(\omega_{2n+1})^2 + c^2}
        &&
        \Psi_{2n+1}(x) = \frac
        {\sin(\omega_{2n + 1}x)}
        {\sqrt{\pi - \frac{\sin(2 \omega_{2n+1} a)}{2\omega_{2n+1}}}}
      \end{align}
      where $c=1/l_c$ and $\omega_n$ are the $n$th root of characteristic
      equations.
  \end{itemize}
\end{frame}
\begin{frame}{Initialisation cont.}
  \fixpdfrgb
  \begin{itemize}
    \item Characteristic are derived from the determinant of KL boundary value
      problem and are given by:
      \begin{gather}
        c - \omega\mathrm{tan}(\omega \pi) = 0,
       \\
       c + \omega\mathrm{tan}(\omega \pi) = 0.
      \end{gather}
    \item KL expansion does not preserve periodicity so finally the KL expansion
      is projected on the Fourier basis with $\sin$ and $\cos$ with appropriate
      frequencies so that to make the function periodic on a given interval.
  \end{itemize}
\end{frame}
\begin{frame}{High dimensional forcing}
  \begin{itemize}
    \item The forcing term is given by the following function
      \small
      \begin{equation}
        f(x, t, \omega) = 
        1 + \sum_{k=1}^5 (-1)^k (\xi_{1,k} \sin(2kx) + \xi_{2,k} \cos(3kx))e^{-\sin(2kt)}
      \end{equation}
      where $\xi_{1,k}$ and $\xi_{2,k}$ for any $k$ are independent random variables
      with standard normal distribution.
    \item The forcing parameter $\sigma$ is set to 0.05.
  \end{itemize}
\end{frame}
\begin{frame}{Reconstructed covariance}
  \fixpdfrgb
  \begin{center}
    \includegraphics[width=0.49\textwidth]{./figures/hd/covarianceN5.pdf}
    \includegraphics[width=0.49\textwidth]{./figures/hd/covarianceN10.pdf}

    Covariance reconstructed from eigen-functions for $N=5$ (left) and $N=10$
    (right).
  \end{center}
\end{frame}
\begin{frame}{$l_c=6$ without forcing} 
  \fixpdfrgb
  \input{./figures/lc6_noforcing/map.pgf}
\end{frame}
\begin{frame}{$l_c=6$ with weak additive forcing} 
  \fixpdfrgb
  \input{./figures/lc6_forcing/map.pgf}
\end{frame}
\begin{frame}{$l_c=0.01$ without forcing} 
  \fixpdfrgb
  \input{./figures/lc0.01_noforcing/map.pgf}
\end{frame}
\begin{frame}{$l_c=0.01$ with weak additive forcing} 
  \fixpdfrgb
  \input{./figures/lc0.01_forcing/map.pgf}
\end{frame}
\section{Summary}
\begin{frame}{Outline}
  \fixpdfrgb
  \tableofcontents[currentsection,hideothersubsections]
\end{frame}
\begin{frame}{Summary}
  \fixpdfrgb
  \begin{block}{Conclusions}
    \begin{itemize}
      \item gPC and PDF approaches converge to the moments of the Monte-Carlo
        simulation.
      \item gPC and PDF allow to significantly decrease computational times.
      \item One point PDF does not encode sufficient information about shock
        statistics.
      \item gPC convergence improves when orthonormal basis is used.
    \end{itemize}
  \end{block}
  \begin{block}{Future work}
    \begin{itemize}
      \item A paper with a two-fluid model.
      \item Proposal with the focus on applications. 
    \end{itemize}
  \end{block}
\end{frame}
\end{document}

