from proteus.iproteus import *
Profiling.logLevel = 1
Profiling.verbose = True
from numpy.random import randn
import numpy as np

import burgers_1d_p as physics
import burgers_1d_dgp2_lim_n as numerics
pList = [physics]
nList = [numerics]
so = default_so
so.sList = [default_s]
opts.cacheArchive = True
so.archiveFlag = Archiver.ArchiveFlags.EVERY_SEQUENCE_STEP

import gauss_hermite_quadrature_data

class sineIC:
    def __init__(self, eta):
        self.eta = eta

    def uOfXT(self, x, t):
        return self.eta + np.sin(x[0])


def forcing_term(x, t, xi):
    return xi*np.sin(t)

for n in range(1,21):
    collocationPoints = gauss_hermite_quadrature_data.nodeDict[n]

    directory = "data{0:02d}".format(n)
    if not os.path.exists(directory):
        os.makedirs(directory)

    # p1 basis
    # Calculate solutions on collocation tensor product mesh
    # Loop over forcing terms stochastics
    j = 0
    for Z in collocationPoints:
        i = 0
        j = j + 1
        # Loop over initial condition stochastics
        for Y in collocationPoints:
            i = i + 1
            runname  = "data{0:02d}/colloc_run_{1:03d}_{2:03d}".format(n,i,j)
            filename = runname + "0.xmf"
            if not os.path.isfile(filename):
                print "Running: ", runname
                so.name = pList[0].name = runname
                xi = np.pi / 10 * Z
                eta = Y
                pList[0].coefficients = pList[0].ForcedBurgersEqn(
                    nu=1e-6,
                    rofx=lambda x, t: forcing_term(x, t, xi),
                    sigma=1
                    )
                pList[0].initialConditions = {0: sineIC(eta)}
                
                ns = NumericalSolution.NS_base(so, pList, nList, so.sList, opts)
                ns.calculateSolution(so.name)
                del ns
