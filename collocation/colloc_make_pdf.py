from proteus.iproteus import *
from proteus import Archiver
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle
from numpy.random import randn

import os
import errno
import gauss_hermite_cubature


def read_from_hdf5(hdfFile, label, dof_map=None):
    """
    Just grab the array stored in the node with label label and return it
    If dof_map is not none, use this to map values in the array
    If dof_map is not none, this determines shape of the output array
    """
    assert hdfFile is not None  # requires hdf5 for heavy data"
    vals = hdfFile.getNode(label).read()
    if dof_map is not None:
        dof = vals[dof_map]
    else:
        dof = vals

    return dof


def post_process_case(case_name):
    archive = Archiver.XdmfArchive(".", case_name, readOnly=True)
    # Extract the number of time steps
    # Apparently Proteus creates a collection of grids
    M = 0
    for a in archive.tree.getroot().iter("Grid"):
        if a.attrib['GridType'] == "Collection":
            if a.attrib['Name'] == "Mesh_dgp2_Lagrange":
                Time = list(enumerate(a.iter("Time")))
                M = len(Time)

    # Obtain the position and velocities
    label_base = "/{0:s}{1:d}"

    x = read_from_hdf5(
        archive.hdfFile, label_base.format("nodesSpatial_Domain", 0))[0:-1, 0]
    u = read_from_hdf5(archive.hdfFile, label_base.format("u", M - 1))
#    archive.close()
    return u[0:-1:3], x


def get_expansion_coeffs(n):

    idxSet = gauss_hermite_cubature.multiindex(n-1)

    # Get the solutions
    for j in range(0,n):
        # Loop over initial condition stochastics
        for i in range(0,n):

            file_name = "data{0:02d}/colloc_run_{1:03d}_{2:03d}".format(n,i+1,j+1)
            print("Post-processing {0}...".format(file_name)),
            u, x_result = post_process_case(file_name)
            if j == 0 and i == 0:
                u_results = u
            else:
                u_results = np.vstack((u_results,u))

            print 'done.'

    w = []
    for ix in range(0,len(x_result)):
        ua = []
        k = 0
        for j in range(0,n):
            uy = []
            # Loop over initial condition stochastics
            for i in range(0,n):
                uy.append(u_results[k][ix])
                k = k + 1
            ua.append(uy)

        w.append(gauss_hermite_cubature.expansionCoeffs(n,idxSet, ua))

    return(w,x_result,idxSet)


def construct_colloc_histogram(N_x=50, N_a=50):
    # Create the bins
    bin_bounds_x = np.linspace(0, 2 * np.pi, num=N_x)
    bin_bounds_a = np.linspace(-4, 4, num=N_a)
    X, A = np.meshgrid(bin_bounds_x, bin_bounds_a)
    H = np.zeros((N_a, N_x))

    # Get the expansion coefficients
    w1,x_result,idxSet1 = get_expansion_coeffs(19)
    w2,x_result,idxSet2 = get_expansion_coeffs(20)

    for N in range(0,100000):
        if N % 100 == 0:
            print "N = ", N
        Y = randn()
        Z = randn()

        u_result = []
        for i in range(0,len(x_result)):
            v1 = gauss_hermite_cubature.expansionValue(Y,Z,w1[i],idxSet1)
            v2 = gauss_hermite_cubature.expansionValue(Y,Z,w2[i],idxSet2)
            u_result.append( 0.5*(v1+v2) )

        Hc, xedges, yedges = np.histogram2d(x_result, u_result, bins=[N_x, N_a], range=[[0, 2*np.pi], [-4,4]])
        H = H + Hc.T

    with open('colloc_histogram.pickle', 'w') as f:
        pickle.dump([X, A, H], f)

construct_colloc_histogram(100, 200)
