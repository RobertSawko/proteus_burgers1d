from proteus.iproteus import *
from proteus import Archiver
import pickle

def read_from_hdf5(hdfFile, label, dof_map=None):
    """
    Just grab the array stored in the node with label label and return it
    If dof_map is not none, use this to map values in the array
    If dof_map is not none, this determines shape of the output array
    """
    assert hdfFile is not None  # requires hdf5 for heavy data"
    vals = hdfFile.getNode(label).read()
    if dof_map is not None:
        dof = vals[dof_map]
    else:
        dof = vals

    return dof


def post_process_case(case_name):
    archive = Archiver.XdmfArchive(".", case_name, readOnly=True)
    # Extract the number of time steps
    # Apparently Proteus creates a collection of grids
    M = 0
    for a in archive.tree.getroot().iter("Grid"):
        if a.attrib['GridType'] == "Collection":
            if a.attrib['Name'] == "Mesh_dgp2_Lagrange":
                Time = list(enumerate(a.iter("Time")))
                M = len(Time)

    # Obtain the position and velocities
    label_base = "/{0:s}{1:d}"

    x = read_from_hdf5(
        archive.hdfFile, label_base.format("nodesSpatial_Domain", 0))[0:-1, 0]
    u = read_from_hdf5(archive.hdfFile, label_base.format("u", M - 1))
    archive.close()
    return u[0:-1:3], x


def get_result(n):
    """
    Load results from the data directory no. n, i.e., get the precalculated
    solution at the cubature nodes.  The results for all grid points are 
    stored in a dictionary for loop-up on index tuple for node.  
    The number of cubature nodes and the mesh for the solution is also 
    stored in the dictionary.
    """

    # Start with a dictionary containing only n
    u = {"n": n}

    # Loop over forcing term stochastics
    for j in range(0,n):
        # Loop over initial condition stochastics
        for i in range(0,n):
            
            file_name = "data{0:02d}/colloc_run_{1:03d}_{2:03d}".format(n,i+1,j+1)
            print("Post-processing {0}...\n".format(file_name)),
            u[i,j], x = post_process_case(file_name)

    u["x"] = x
    return(u)

u_results = {}
for n in range(1,21):
    u_results[n] = get_result(n)

with open('colloc_u_results.pickle', 'w') as f:
    pickle.dump(u_results, f)
