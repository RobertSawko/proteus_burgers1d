import math
import gauss_hermite_quadrature_data

def multiindex (n):
    """
    Set up a multiindex for two random dimensions as a list of tuples (i,j) in 
    graded lexiographic order.
    """
    idxList = []
    for j in range(0, n + 1):
        for i in range(j, -1, -1):
            idxList.append((i,j - i))
    return(idxList)

def hermitevalue(nmax,x):
    """
    Calculate a list of the values of the Hermite polynomials of order up to
    nmax evaluated at the point x.
    """
    vals = []
    for n in range(0,nmax+1):
        if n == 0:
            vals.append(1)
        elif n == 1:
            vals.append(x)
        else:
            vals.append(x*vals[n-1]-(n-1)*vals[n-2])
        
    return(vals)

def expansionCoeffs(n, idxSet, u):
    """
    Evaluate the Gauss-Hermite cubature (tensor product of 1D Gauss-Hermite 
    quadrature) to find the gPC expansion for u in two standard normal 
    distributed random variables Y and Z according to Eq. 7.17 in D. Xiu, 
    Numerical Methods for Stochastic Computations, 2010.

    n      - number of points in quadrature in one direction
    idxSet - indices for basis polynomials (k,l)
    u      - values at quadrature points for function to approximate
    """

    # Look up the quadrature nodes and weights
    nodes = gauss_hermite_quadrature_data.nodeDict[n]
    alpha = gauss_hermite_quadrature_data.weightDict[n]

    # Maximum polynomial order
    m = max(idxSet,key=lambda item:item[0])[0]

    # Calculate values of all Hermite polynomials in one variable in all nodes
    # We use the same mesh in Y and Z directions.  
    H = []
    for node in nodes:
        H.append(hermitevalue(m,node))

    # Evaluate the cubature and append to the list of expansion coefficients w
    w = []
    for idx in idxSet:
        kY = idx[0]   # Order of polynomial in Y
        kZ = idx[1]   # Order of polynomial in Z
        s = 0
        for iZ in range(0,n):
            for iY in range(0,n):
                s = s + alpha[iY]*alpha[iZ]*H[iY][kY]*H[iZ][kZ]*u[iZ][iY]
                
        gamma = math.factorial(kY)*math.factorial(kZ)
        s     = s/math.pi/gamma
        w.append(s)

    return(w)

def expansionValue(Y,Z,w,idxSet):
    """
    Calculate the expansion in Eq. 7.16 (D. Xiu, Stochastic ..., 2010)
    """
    # Maximum polynomial order
    n = max(idxSet,key=lambda item:item[0])[0]

    HY = hermitevalue(n,Y)
    HZ = hermitevalue(n,Z)

    u = 0
    for k in range(0,len(idxSet)):
        kY = idxSet[k][0]
        kZ = idxSet[k][1]
        u = u + w[k]*HY[kY]*HZ[kZ]

    return(u)

"""
# Check cubature.  Diagonal should be one, all other zero, due to orthogonality
n=10
m=4
iset = multiindex(m)
print(iset)
for k in iset:
    u = []
    for Z in gauss_hermite_quadrature_data.nodeDict[n]:
        uY=[]
        for Y in gauss_hermite_quadrature_data.nodeDict[n]:
        
            HY = hermitevalue(m,Y)[k[0]]
            HZ = hermitevalue(m,Z)[k[1]]
            uY.append(HY*HZ)
            #uY.append((Y**2-1)*Z)
        u.append(uY)

    w    = expansionCoeffs(n,iset,u)
    s = ""
    for val in w:
        s = s + "{0:6.2f}".format(val) 
    print k, s
"""
