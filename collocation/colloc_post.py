import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle

import os
import errno
import gauss_hermite_quadrature_data
import gauss_hermite_cubature
from math import factorial, sqrt

def expectation(u):
    """
    Calculate expectation for the results in u  into the list E with one value 
    for each mesh point.
    """
    
    n = u["n"]
    E = []
    # Loop over mesh-points indices to set up a list of lists for the solution
    # u at each mesh-point representing the solution at each node in the random space
    for ix in range(0,len(u["x"])):
        ull = [ [u[i,j][ix] for i in range(0,n)] for j in range(0,n)]

        idxSet = gauss_hermite_cubature.multiindex(1)
        w      = gauss_hermite_cubature.expansionCoeffs(n,idxSet,ull)
        E.append(w[0])

    return(E)

def mc_extract_mean(X, A, H):
    Xb = np.sort(np.unique(X[0, :]))
    mean = np.zeros(len(Xb))

    for i, xb in enumerate(Xb):
        a = A[:, i]
        pdf = H[:, i]
        mean[i] = np.inner(a.T, pdf.T) / np.sum(pdf)

    return Xb, mean

X = np.genfromtxt('X.csv', delimiter=',')
A = np.genfromtxt('A.csv', delimiter=',')
H = np.genfromtxt('H.csv', delimiter=',')

xmc,umcmean = mc_extract_mean(X,A,H)

gpcN8 = np.genfromtxt('gpcN8.csv', delimiter=',')

with open('colloc_u_results.pickle') as f:
    u_results = pickle.load(f)

fig   = plt.figure()
lines = []
nvals = range(3,21,4)
Evals = []
for n in nvals:
    u = u_results[n]
    E = expectation(u)
    l,= plt.plot(u["x"],E)
    lines.append(l)
    Evals.append(E)

linetags = ["n={0:02d}".format(n) for n in nvals]

#l,=plt.plot(gpcN8[:,14],gpcN8[:,0])
#lines.append(l)
#linetags.append("gpcN8")

l,=plt.plot(xmc,umcmean,'k')
l.set_linewidth(2)
lines.append(l)
linetags.append("MC")

legend   = fig.legend(lines,linetags)

fig.suptitle("Pseudospectral collocation gPC odd no. of cubature nodes")
ax = fig.gca()
ax.set_xticks((0, np.pi, 2*np.pi))
ax.set_xticklabels(("0", "$\pi$", "$2\pi$"))
ax.set_xlabel("$x$")
ax.set_ylabel("$a$")

fig.savefig("gpc_colloc_odd.pdf", bbox_inches='tight', transparent=True)

plt.clf()
lines = []
nvals = range(4,21,4)
Evals = []
for n in nvals:
    u = u_results[n]
    E = expectation(u)
    l,= plt.plot(u["x"],E)
    lines.append(l)
    Evals.append(E)

linetags = ["n={0:02d}".format(n) for n in nvals]

#l,=plt.plot(gpcN8[:,14],gpcN8[:,0])
#lines.append(l)
#linetags.append("gpcN8")

l,=plt.plot(xmc,umcmean,'k')
l.set_linewidth(2)
lines.append(l)
linetags.append("MC")

legend   = fig.legend(lines,linetags)

fig.suptitle("Pseudospectral collocation gPC even no. of cubature nodes")
ax = fig.gca()
ax.set_xticks((0, np.pi, 2*np.pi))
ax.set_xticklabels(("0", "$\pi$", "$2\pi$"))
ax.set_xlabel("$x$")
ax.set_ylabel("$a$")

fig.savefig("gpc_colloc_even.pdf", bbox_inches='tight', transparent=True)

plt.clf()
lines = []
nvals = range(3,21,4)
Evals = []
for n in nvals:
    u  = u_results[n]
    E1 = expectation(u)
    u  = u_results[n+1]
    E2 = expectation(u)
    E  = []
    for i in range(len(E1)):
        E.append(0.5*(E1[i]+E2[i]))
    l,= plt.plot(u["x"],E)
    lines.append(l)
    Evals.append(E)

linetags = ["n={0:02d}+{1:02d}".format(n,n+1) for n in nvals]

#l,=plt.plot(gpcN8[:,14],gpcN8[:,0])
#lines.append(l)
#linetags.append("gpcN8")

l,=plt.plot(xmc,umcmean,'k')
l.set_linewidth(2)
lines.append(l)
linetags.append("MC")

legend   = fig.legend(lines,linetags)

fig.suptitle("Pseudospectral collocation gPC odd/even average")
ax = fig.gca()
ax.set_xticks((0, np.pi, 2*np.pi))
ax.set_xticklabels(("0", "$\pi$", "$2\pi$"))
ax.set_xlabel("$x$")
ax.set_ylabel("$a$")
plt.ylim((-0.8,0.8))

fig.savefig("gpc_colloc_avg.pdf", bbox_inches='tight', transparent=True)
