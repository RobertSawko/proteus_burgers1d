import numpy as np

def mc_extract_mean_and_std(X, A, H):
    Xb = np.sort(np.unique(X[0, :]))
    mean = np.zeros(len(Xb))

    for i, xb in enumerate(Xb):
        a = A[:, i]
        pdf = H[:, i]
        mean[i] = np.inner(a.T, pdf.T) / np.sum(pdf)

    return Xb, mean

X = np.genfromtxt('X.csv', delimiter=',')
A = np.genfromtxt('A.csv', delimiter=',')
H = np.genfromtxt('H.csv', delimiter=',')

x,umean =mc_extract_mean_and_std(X,A,H)

print x
print umean
