import proteus.default_p as p
from proteus.iproteus import *
import proteus as pr
import numpy as np
from proteus.TransportCoefficients import TC_base
Profiling.verbose = True
Profiling.logLevel = 7
# from proteus import Domain
p.nd = 2

p.nd = 2
p.L = (2 * np.pi, 8)
p.name = "kinetic"
#p.domain = Domain.RectangularDomain(L=(2 * np.pi, 8), x=[0, -4], name=p.name)


class KineticEqn(TC_base):
    def __init__(self, nu=0.0, sigma=0.05, fofx=lambda x, t: 0):
        nc = 2
        mass = {}
        advection = {}
        diffusion = {}
        potential = {}
        reaction = {}
        hamiltonian = {}
        mass[0] = {0: 'constant'}
        mass[1] = {1: 'linear'}
        reaction[0] = {0: 'constant'}
        reaction[1] = {0: 'linear'}
        # This is telling how many arguments you've got! so essentially the
        # line belows says "hamiltonian for equation 0 is a linear function of
        # gradient of 0th variable and linear function of a gradient of 1st
        # variable."
        hamiltonian[0] = {0: 'linear', 1: 'linear'}
        hamiltonian[1] = {1: 'linear'}
        TC_base.__init__(self,
                         nc=nc,
                         mass=mass,
                         diffusion=diffusion,
                         potential=potential,
                         reaction=reaction,
                         hamiltonian=hamiltonian,
                         variableNames=['I', 'p'])
        self.nu = nu
        self.fofx = fofx
        self.sigma = sigma

    def evaluate(self, t, c):
        I = c[('u', 0)]
        p = c[('u', 1)]
        x = c['x'][..., 0]
        a = c['x'][..., 1] - 4
        gradI = c['grad(u)', 0]
        gradp = c['grad(u)', 1]

        c[('m', 0)][:] = 0

        c[('m', 1)][:] = p
        c[('dm', 1, 1)][:] = 1

        c[('H', 0)][:] = gradI[..., 1] - gradp[..., 0]
        # derivatives with respect  to _gradient_ on hamiltonian terms
        c[('dH', 0, 0)][..., 0] = 0
        c[('dH', 0, 0)][..., 1] = 1.0
        c[('dH', 0, 1)][..., 0] = -1.0
        c[('dH', 0, 1)][..., 1] = 0

        c[('H', 1)][:] =\
            a * gradp[..., 0] + self.sigma * self.fofx(x, t) * gradp[..., 1]
        c[('dH', 1, 1)][..., 0] = a
        c[('dH', 1, 1)][..., 1] = self.sigma * self.fofx(x, t)

        c[('r', 0)][:] = 0

        c[('r', 1)][:] = I
        c[('dr', 1, 0)][:] = 1


def sine_forcing(x, t, xi):
    f = xi * np.sin(t)
    return f

xi = 0  # np.random.randn()

p.coefficients = KineticEqn(
    nu=1e-6, sigma=1, fofx=lambda x, t: sine_forcing(x, t, xi))


class pdf_IC:
    def __init__(self, b):
        self.b = b
        # Treat the constant jointly
        self.C = 5.0 / np.pi**2 * np.exp(-50.0 * b**2 / np.pi**2)

    def uOfXT(self, x, t):
        a = x[1] - 4
        # print a
        return self.C * np.exp(-(a - np.sin(x[0]))**2 / 2)


class I_IC:
    def __init__(self, b):
        pass
        self.pi2 = np.pi**2
        self.C = -5 / self.pi2
        self.b = b

    def uOfXT(self, x, t):
        a = x[1] - 4
        return self.C * np.cos(x[0])\
            * np.exp(
                - (np.pi**2 * np.sin(x[0])**2
                    - 2 * self.pi2 * np.sin(x[0]) * a
                    + self.pi2 * a**2 + 100 * self.b**2)
                / (2 * self.pi2))

p.initialConditions = {0: I_IC(xi),
                       1: pdf_IC(xi)}


def emptyBC(x, flag):
    return None


def dirichletZeroBC(x, flag):
    a = x[1] - 4
    if np.abs(a) == 4:
        return lambda x, t: 0.0
    else:
        return None

p.dirichletConditions = dict((i, dirichletZeroBC) for i in range(2))
p.advectiveFluxBoundaryConditions = dict((i, emptyBC) for i in range(2))
p.diffusiveFluxBoundaryConditions = dict((i, {}) for i in range(2))


def getPBC(x, flag):
    eps = 1.0e-8
    if x[0] < eps or x[0] >= p.L[0] - eps:
        return np.array([0.0, x[1], 0.0])

p.periodicDirichletConditions = dict((i, getPBC) for i in range(2))
# p.fluxBoundaryConditions = dict((i, 'outFlow') for i in range(2))

p.T = 1.0

# Numerical setup starts here
import proteus.default_n as n

nSteps = 100
dt = p.T/float(nSteps+1)
n.tnList = [i*dt for i in range(nSteps+1)]

n.timeOrder = 3
n.nStagesTime = n.timeOrder

n.DT = None
n.runCFL = 0.5

n.limiterType = pr.TimeIntegration.DGlimiterP2Lagrange2d  # None

# n.timeIntegration = pr.TimeIntegration.SSPRKPIintegration
# n.stepController = pr.StepControl.Min_dt_RKcontroller
n.timeIntegration = pr.TimeIntegration.BackwardEuler
n.stepController = pr.StepControl.FixedStep
n.nDTout = 10

n.femSpaces = dict(
    (i, pr.FemTools.DG_AffineQuadraticOnSimplexWithNodalBasis)
    for i in range(2))

n.elementQuadrature = pr.Quadrature.SimplexGaussQuadrature(p.nd, 4)
n.elementBoundaryQuadrature = pr.Quadrature.SimplexGaussQuadrature(p.nd-1, 4)

n.nnx = 41
n.nny = 56
n.nLevels = 1

n.subgridError = None
n.massLumping = False

n.numericalFluxType = pr.NumericalFlux.HamiltonJacobi_DiagonalLesaintRaviart

n.shockCapturing = None
n.multilevelNonlinearSolver = n.NLNI

n.usingSSPRKNewton = False
n.levelNonlinearSolver = n.Newton

n.nonlinearSmoother = n.NLGaussSeidel

n.fullNewtonFlag = True

n.tolFac = 0.0

n.nl_atol_res = 1.0e-4

n.matrix = n.SparseMatrix

n.multilevelLinearSolver = n.LU

n.levelLinearSolver = n.LU

n.linearSmoother = n.GaussSeidel

n.linTolFac = 0.001
n.l_atol_res = 0.001*n.nl_atol_res

n.conservativeFlux = None

n.checkMass = True

n.periodicDirichletConditions = dict((i, getPBC) for i in range(2))
n.parallelPeriodic = False
n.parallelPartitioningType = n.MeshParallelPartitioningTypes.element
n.nLayersOfOverlapForParallel = 0

from proteus import default_s, default_so

so = default_so
so.name = p.name
so.sList = [default_s]
so.tnList = n.tnList

ns = NumericalSolution.NS_base(so, [p], [n], so.sList, opts)
ns.calculateSolution("kinetic")
