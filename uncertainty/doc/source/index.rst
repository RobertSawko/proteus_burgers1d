.. KL expansion documentation master file, created by
   sphinx-quickstart2 on Sat Jan 17 16:29:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KL expansion's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: kl_expansion
    :members:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

